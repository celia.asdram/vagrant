# vagrant

Vagrant init (versionlinux) --> fait savoir au logiciel quelle version de linux charger + créer le fichier Vagrantfile
Vagrant validate --> confirmer la creation du fichier
Vagrant up --> set up la vm dans virtualbox
Vagrant status --> etat de la machine
Vagrant global-status --> etat global machine
Vagrant ssh --> se connecte a la vm en ssh via terminal
Vagrant halt --> eteindre la vm
Vagrant destroy --> detruit la machine
Vagrant box add ubuntu/trusty64 --> ajoute la version de cette machine dans le fichier Vagrantfile
Vagrant init ubuntu/trusty64 -h --> liste de parametres par rapport à cette version de linux sur vagrant
--> Vagrant init (versionlinux) --> fait savoir au logiciel quelle version de linux charger + créer le fichier Vagrantfile
Vagrant validate --> confirmer la creation du fichier
Vagrant up --> set up la vm dans virtualbox
Vagrant status --> etat de la machine
Vagrant global-status --> etat global machine
Vagrant ssh --> se connecte a la vm en ssh via terminal
Vagrant halt --> eteindre la vm
Vagrant destroy --> detruit la machine
Vagrant box add ubuntu/trusty64 --> ajoute la version de cette machine dans le fichier Vagrantfile
Vagrant init ubuntu/trusty64 -h --> liste de parametres par rapport à cette version de linux sur vagrant

--------------------------------------------------LAB2---------------------------------------------------
Lab-2 : Créer un vbox
Créez un dossier lab-2 --> sudo mkdir LAB2
Déplacez-vous dans ce dossier --> cd LAB2
Initialisez vagrant en utilisant la version 20190425.0.0 ubuntu/trusty64 --> vagrant init ubuntu/trusty64
Démarrez la VM --> vagrant up
Connectez-vous en ssh --> vagrant ssh
Installer nginx --> apt install nginx
Démarrez le service et activez son démarrage au lancement de la vm --> update -rc.d nginx enable 3 4 5
Créez une vbox à partir de cette CM --> vagrant package --base my-virtual-machine --output nouveau_nom_plus_simple
Créez un compte sur vagrant cloud
Publiez votre vbox sur vagrant cloud sous le nom <username>/nginx version v1

--------------------------------------------------LAB3-------------------------------------------------------
Lab-3 : Création d’un Vagrantfile
Créez le dossier lab-3
Déplacez-vous dans ce dossier
Créez un vagrantfile minimaliste (chercher la commande) afin de configurer la VM à partir
des informations suivantes
image de base centos 7 by geerlingguy
(( vagrant init geerlingguy/centos7 -m ))
cpu : 2
ram : 2 Go
Variabiliser les paramètres indiquées ci-dessus
---> edition Vagrantfile
------> config  
 config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048" # 2 Go de RAM
    vb.cpus = 1 # 1 CPU
    ---------------------------------------------
Connection ssh --> vagrant ssh
Installer nginx --> CENTOS: sudo yum install epel-release && sudo yum install nginx
Arretez la vm puis supprimez la
--> vagrant halt
--> vagrant destroy
-----------------------------------------LAB4----------------------------------------------
Lab-4 : Déploiement d’un serveur web
Créez le dossier lab-4
Créez un vagrantfile avec les paramètres suivants
image de base : centos 7 by geerlingguy
cpu : 2
ram : 2
IP fixe privée : 10.0.0.10
----> modifier Vagrantfile 
---------> config.vm.network "forwarded_port", guest: 80, host: 8080
 ((redirection de port pour ngminx))
 --------> config.vm.network "private_network", ip: "10.0.0.10"
 ((ip statique en 10.0.0.1))
                    !!!!DECOMMENTER SINON CA MARCHE PA!!!!!!!!!
Variabiliser les paramètres ci-dessus
Connection ssh
Installer nginx
Vérifiez que vous avez accès à l’application depuis le PC local via http://10.0.0.10 -> oui
Arretez la vm puis la supprimer

----------------------------------------LAB5-----------------------------------------------

Lab-5 : Déploiement d’un serveur Web
Créez un dossier lab-5
Créez un vagrantfile afin de configurer 3 VMs à partir des infos suivantes
image de base : ubuntu/xenial64
cpu : 1
ram : 1 go

VM1 : lb, ip: 10.0.0.10
# VM1 : lb, ip: 10.0.0.10
config.vm.define "lb" do |lb|
  lb.vm.hostname = "lb"
  lb.vm.network "private_network", ip: "10.0.0.10"
end

VM2 : web1, ip: 10.0.0.11
# VM2 : web1, ip: 10.0.0.11
config.vm.define "web1" do |web1|
  web1.vm.hostname = "web1"
  web1.vm.network "private_network", ip: "10.0.0.11"
  web1.vm.provision "shell", inline: <<-SHELL
    apt-get update 
    apt-get install -y apache2
  SHELL
end

VM3 : web2, ip: 10.0.0.12
# VM3 : web2, ip: 10.0.0.12
config.vm.define "web2" do |web2|
  web2.vm.hostname = "web2"
  web2.vm.network "private_network", ip: "10.0.0.12"
  web2.vm.provision "shell", inline: <<-SHELL
    apt-get update 
    apt-get install -y apache2
  SHELL
end

Bien sur les VM 2 et 3 ont une page web actif --> oui 
Connection ssh sur chaque machine
Arrêtez les VMs puis supprimez les

---------------------------------------LAB6-------------------------------------------------
Lab-6 : Vagrant plugins
Créez un dossier lab-6
Créez un vagrantfile afin de configurer 3 VMs à partir des informations suivantes
image de base : ubuntu/xenial64
cpu : 1
ram : 1 Go
VM1: lb, ip: 10.0.0.10
VM2: web1, ip: 10.0.0.11
VM3: web2, ip: 10.0.0.12
Installez et utilisez le plugin vagrant-hostupdater [ - afin que le fichier hosts de chaque
machine corresponde à la réalité de leur nom et de leur ip malgré les actions UP, RESUME
et RELOAD (lisez la doc pour mieux comprendre son utilité) - ]

vagrant 

Lors du vagrant up vérifier que le plugin est bien appelé
Connection ssh
Arrêtez les VM puis supprimez les

-------------------------------------LAB7--------------------------------------------------

Créez le dossier lab-7
Créez un vagrantfile afin de configurer 1 VM à partir des infos suivantes
image de base : <username>/nginx (Box lab-2)
cpu : 1
ram : 1 Go
IP fixe : 10.0.0.10
Récupérez en local dans votre dossier lab-7 l'application static-website-example
Montez le code de l’application par le moyen de votre choix dans le dossier /var/www/html
de la VM
Vérifier que le site récupéré est bien accessible
Arretez la VM supprimez la

--------------------------------------LAB8----------------------------------------------------

lb.sh:
  màj systeme/packets
  installe nginx
  arrête nginx
  enleve le répertoire de sites par defaut installés par nginx
  création du même répertoire qui sera cette fois ci vide
  ajoute les serveurs des deux autres vm créer dans le vagrantfile via leur ip en local
  redirection de port sur le port 80
  partage de la page web a afficher lorsqu'on ouvre la page via l'IP
  autorisation d'acceder a la page web en local
  mise en place du proxy pour acceder en site en http
  ajoute le site dans les sites par defaut
  allume nginx
  affiche du texte comme quoi cest bon quand tout est fait
end

web 1 et 2.sh:
  installer nginx et le mettre dans le dossier var www html pour afficher la page web quand on se connecte en local

end




## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/celia.asdram/vagrant.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/celia.asdram/vagrant/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.




